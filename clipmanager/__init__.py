#!/usr/bin/env python
# -*- coding: utf-8 -*-

VERSION = (0, 3,)
__version__ = '%s.%s' % (VERSION[0], VERSION[1])